# GitLab Tags

## Description
GitLab Tags Project is designed to focus on creating, displaying, deleting tags locally and remotely through Git.

## Installation
There is no necessity to install it.

## Usage
This Project is used for my personal skills development. 

## Authors and acknowledgment
Dominika

## License
[![MIT](https://img.shields.io/badge/license-MIT-green)](https://choosealicense.com/licenses/mit/)

